export function checkoutData(products, cart){
    const items = []
    cart.forEach(item => {
      const cartProduct = products.find(product => product.article === item.id)
      items.push(`${cartProduct.name} ${cartProduct.price} ₴ ${item.quantity} pcs.`)
    })
    return items
  }