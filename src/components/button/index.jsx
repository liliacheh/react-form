import PropTypes from 'prop-types';

export default function Button({ className, onClick, type = 'button', title, text, children }) {
  return (
    <button className={className} onClick={onClick} type={type} title={title}>
      {text}
      {children}
    </button>
  );
}
Button.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.string,
  title: PropTypes.string,
  text: PropTypes.string,
  children: PropTypes.object,
};
