import React, { useCallback, useEffect } from 'react';
import style from './modal.module.scss';
import PropTypes from 'prop-types';
import { modalProps as modProps } from './modalProps';
import { useDispatch, useSelector } from 'react-redux';
import { setModalType } from '../../redux/actions/modal';
import { setArt } from '../../redux/actions/article';
import { addToCart, removeProductFromCart } from '../../redux/actions/cart';

export default function Modal(props) {
  const modalType = useSelector((state) => state.modalType.modalType);
  const modalProps = modProps.find((modal) => modal.type === modalType);
  const cart = useSelector((state) => state.cart);
  const article = useSelector((state) => state.article);
  const products = useSelector((state) => state.products.products);
  const dispatch = useDispatch();

  const handleAddToCart = () => {
    dispatch(addToCart(products, article, cart));
  };
  const closeModal = useCallback(() => {
    dispatch(setModalType(null));
    dispatch(setArt(null));
  }, [dispatch]);

  const removeFromCart = () => {
    dispatch(removeProductFromCart(cart, article));
    closeModal();
  };
  useEffect(() => {
    function handleClickOutside(e) {
      const modal = document.querySelector('#modal__body');
      if (modal && !modal.contains(e.target)) {
        closeModal();
      }
    }
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [props, closeModal]);

  return (
    <div className={style.modal}>
      <div className={style.modal__body} id="modal__body">
        <div className={style.modal__header}>
          <h3 className={style.modal__title}>{modalProps.header}</h3>
          {modalProps.closeButton && modalProps.closeBtn(style.modal__close, closeModal)}
        </div>
        <div className={style.modal__text}>{modalProps.text}</div>
        <div className={style.modal__btns}>
          {modalProps.actions && modalProps.type === 'buy'
            ? modalProps.actions(style.modal__btn, handleAddToCart, closeModal)
            : modalProps.actions(style.modal__btn, removeFromCart, closeModal)}
        </div>
      </div>
    </div>
  );
}
Modal.propTypes = {
  closeButton: PropTypes.bool,
  closeModal: PropTypes.func,
  addToCart: PropTypes.func,
  removeFromCart: PropTypes.func,
  data: PropTypes.shape({
    type: PropTypes.string,
    header: PropTypes.string,
    text: PropTypes.object,
    closeBtn: PropTypes.func,
    actions: PropTypes.func,
  }),
};
