import style from "./productList.module.scss";
import ProductItem from "../productItem";
import PropTypes from 'prop-types';

export default function ProductList (props){
   
        const { products, type, productNum } = props
        return(
            <div className={style.productList}>
            <div className={style.productList__container}>
                <div className={style.productList__wrapper}>
                    {products.map(product => 
                        <ProductItem
                            key={product.article}
                            product={product}
                            productNum={productNum}
                            type={type} 
                            /> 
                    )}
                </div>
            </div>
        </div>
        )
}


ProductList.propTypes = {
    product: PropTypes.array,
    productNum: PropTypes.bool,
    type: PropTypes.string
}
