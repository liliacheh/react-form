import { useSelector } from "react-redux";
import { FavIcon, CartIcon} from "../icons"
import style from "./header.module.scss"
import { NavLink } from "react-router-dom";

export default function Header() {
  
  const favorites = useSelector( state => state.favorites).length
  const cart = useSelector( state => state.cartQuantity)

    return (
      <>
        <header className={style.header}>
          <div className={style.header__container}>
            <div className={style.header__wrapper}>
              <NavLink to='/' className={style.header__logo}>
                <img className={style.header__img} src="./logo.svg" alt="logo" />
              </NavLink>
              <div className={style.header__btns}>
                <NavLink to='/favorites' className={style.header__favorites} title="Favorites">
                  <FavIcon />
                  {favorites > 0 && <div>{favorites}</div>}
                </NavLink>
                <NavLink to='/cart' className={style.header__cart} title="Cart">
                  <CartIcon />
                  {cart > 0 && <div>{cart}</div>}
                </NavLink>
              </div>
            </div>
          </div>
        </header>
      </>
    );
  }
 

