import * as Yup from 'yup';

export const validationSchema = Yup.object({
    firstName: Yup.string()
      .max(15, 'Must be 15 characters or less')
      .min(2, 'Must be 2 characters or more')
      .required('Required'),
    lastName: Yup.string()
      .min(2, 'Must be 2 characters or more')
      .max(20, 'Must be 20 characters or less')
      .required('Required'),
    age: Yup.number().integer().positive().required('Required'),
    address: Yup.string().required('Required'),
    phone: Yup.string()
      .matches(/^\(\d{3}\)\d{3}-\d{2}-\d{2}$/, 'Mobile phone must be in the format (###) ###-##-##')
      .required('Required'),
  })