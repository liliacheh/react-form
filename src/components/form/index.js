import { Formik, Field, Form, ErrorMessage } from 'formik';
import style from './form.module.scss';
import { PatternFormat } from 'react-number-format';
import { validationSchema } from './validation';
import { initialValues } from './initialValues';
import { useDispatch, useSelector } from 'react-redux';
import { checkout } from '../../redux/actions/cart';


const CheckoutForm = () => {
    const dispatch = useDispatch();
    const products = useSelector(state => state.products.products)
    const cart = useSelector(state => state.cart)
    const handleCheckout = (values) => {
        dispatch(checkout(values,products,cart))
      }
  return (
    <div className={style.formContainer}>
        <h2 className={style.formTitle}>Contact information</h2>
      <Formik
        initialValues = {initialValues}
        validationSchema = {validationSchema}
        onSubmit={handleCheckout}
      >
        {({ touched, errors, values, handleChange, handleBlur }) => (
          <Form>
            <label htmlFor="firstName">First Name:</label>
            <Field
              name="firstName"
              type="text"
              className={touched.firstName && errors.firstName ? `${style.input} ${style.error}` : style.input}
            />
            <ErrorMessage name="firstName" component="div" className={style.errorMessage} />

            <label htmlFor="lastName">Last Name:</label>
            <Field
              name="lastName"
              type="text"
              className={touched.lastName && errors.lastName ? `${style.input} ${style.error}` : style.input}
            />
            <ErrorMessage name="lastName" component="div" className={style.errorMessage} />

            <label htmlFor="age">Age:</label>
            <Field
              name="age"
              type="number"
              className={touched.age && errors.age ? `${style.input} ${style.error}` : style.input}
            />
            <ErrorMessage name="age" component="div" className={style.errorMessage} />

            <label htmlFor="address">Address:</label>
            <Field
              name="address"
              type="text"
              className={touched.address && errors.address ? `${style.input} ${style.error}` : style.input}
            />
            <ErrorMessage name="address" component="div" className={style.errorMessage} />

            <label htmlFor="phone">Phone:</label>
            
            <PatternFormat
              format="(###)###-##-##"
              mask="_"
              name="phone"
              value={values.phone}
              onChange={handleChange}
              onBlur={handleBlur}
              className={touched.phone && errors.phone ? `${style.input} ${style.error}` : style.input}
            />
            <ErrorMessage name="phone" component="div" className={style.errorMessage} />

            <button className={style.submitBtn} type="submit">
              Checkout
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};
export default CheckoutForm;
