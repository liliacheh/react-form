import { createBrowserRouter } from 'react-router-dom'
import Home from '../pages/Home/home'
import { Favorites } from '../pages/Favorites/Favorites'
import { NotFound } from '../pages/NotFound/NotFound'
import { Cart } from '../pages/Cart/Cart'
import { Layout } from '../pages/Layout/Layout'

// export default function Router() {
//     return (
//         <Routes>
//             <Route path='/' element={<Layout/>}>
//                 <Route index element={<Home />} />
//                 <Route path='/cart' element={<Cart />} />
//                 <Route path='/favorites' element={<Favorites />} />
//             </Route>
//             <Route path='*' element={<NotFound />} />
//         </Routes >

        
//     )
// }
export const router = createBrowserRouter([
    {
        path : "/",
        element : <Layout/>,
        errorElement: <NotFound text='Page not found'/>,
        children: [
            {
                element: <Home/>,
                index: true
            },
            {
                element: <Cart/>,
                path: '/cart',
                errorElement: <NotFound text='Page not found'/>
            },
            {
                element: <Favorites/>,
                path : "/favorites",
                errorElement: <NotFound text='Page not found'/>
            }
        ] 
    }
])