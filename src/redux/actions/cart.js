import { cartTypes } from '../types';
import { setModalType } from './modal';
import { checkoutData } from '../../functions';

export function addNewItemToCart(cartItem) {
  return {
    type: cartTypes.ADD_TO_CART,
    payload: cartItem,
  };
}

export function getCartItems(cart) {
  return {
    type: cartTypes.GET_CART,
    payload: cart
  };
}
export function getCart() {
  let cart = localStorage.getItem('cart');

  try {
    cart = JSON.parse(cart);
  } catch (error) {
    console.error('Cart must be valid JSON');
    return dispatch => dispatch(getCartItems([]));
  }

  if (cart && Array.isArray(cart) && cart.every(item => typeof item === 'object')) {
    return dispatch => dispatch(getCartItems(cart));
  } else {
    console.error('Cart must be an array of objects');
    return dispatch => dispatch(getCartItems([]));
  }
}
    //add product to cart
    export function addToCart (products, article, cart) {
      return dispatch => {
      const newCartItem = products.find(product => product.article === article);
      const itemInCartIndex = cart.findIndex(item => item.id === newCartItem.article);
    
      if (itemInCartIndex === -1) {
        // add new item to cart
        const newItem = { id: newCartItem.article, quantity: 1 };
        const newCart = [...cart, newItem];
        localStorage.setItem('cart', JSON.stringify(newCart));
         dispatch(addNewItemToCart(newItem))
      } else {
        // increment item quantity in cart
        const updatedItem = { ...cart[itemInCartIndex], quantity: cart[itemInCartIndex].quantity + 1 };
        const updatedCart = [...cart];
        updatedCart[itemInCartIndex] = updatedItem;
        localStorage.setItem('cart', JSON.stringify(updatedCart));
        dispatch(increaseItemNumInCart(updatedItem))
      }
       dispatch(setModalType(null))
    }
    };

export function increaseItemNumInCart(newCartItem) {
  return {
    type: cartTypes.INCREASE_ITEM_NUM_IN_CART,
    payload: newCartItem,
  };
}

//increase the quantity of an item in the cart by 1 (+)
export function increaseCartItem (cart, article){
  const itemInCartIndex = cart.findIndex(item => item.id === article);
  const updatedItem = { ...cart[itemInCartIndex], quantity: cart[itemInCartIndex].quantity + 1 };
  const updatedCart = [...cart];
  updatedCart[itemInCartIndex] = updatedItem;
  localStorage.setItem('cart', JSON.stringify(updatedCart));
  return dispatch => dispatch(increaseItemNumInCart(updatedItem))
}

//remove product from the cart
export function removeProductFromCart(cart, article) {
    const updatedCart = cart.filter(item => item.id !== article);
    localStorage.setItem('cart', JSON.stringify(updatedCart));
    return {
      type: cartTypes.REMOVE_FROM_CART,
      payload: updatedCart
    };
  }
  
export function decreaseItemNumInCart(cartItem) {
  return {
    type: cartTypes.DECREASE_ITEM_NUM_IN_CART,
    payload: cartItem,
  };
}

//remove a single item from the cart
export function removeSingleItemFromCart (cart, article){
  const itemIndex = cart.findIndex(item => item.id === article);
  if (itemIndex !== -1) {
    const item = cart[itemIndex];
    if (item.quantity > 1) {
      const updatedItem = {...item, quantity: item.quantity - 1};
      const updatedCart = [...cart];
      updatedCart[itemIndex] = updatedItem;
      localStorage.setItem('cart', JSON.stringify(updatedCart));
      return dispatch =>  dispatch(decreaseItemNumInCart(updatedItem))
    } else {
      const updatedCart = cart.filter(item => item.id !== article);
      localStorage.setItem('cart', JSON.stringify(updatedCart));
      return dispatch =>  dispatch(removeProductFromCart(cart, article));
    }
  }
}

function clearCart() {
  return {
    type: cartTypes.CLEAR_CART,
  };
}
export function checkout(values, products, cart) {
  return (dispatch) => {
    console.log('Checkout:', values, 'cart:', checkoutData(products, cart) );
    localStorage.setItem('cart', '[]');
    dispatch(clearCart());
    alert('Thank you for your purchase');
  };
}

