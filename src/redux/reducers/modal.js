import { modalTypes } from "../types";

const initialState = {
    modalType: null
}
export const modalReducer = (state = initialState, action) => {
    switch (action.type){
        case modalTypes.GET_MODAL_TYPE:
            return state
        case modalTypes.SET_MODAL_TYPE:
            return {...state, modalType:action.payload}
        default:
            return state
    }   
}