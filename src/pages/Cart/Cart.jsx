import ProductList from "../../components/productList"
import { Loader } from "../../components/loader";
import { useDispatch, useSelector } from "react-redux";
import { NotFound } from "../NotFound/NotFound";
import { getCart } from "../../redux/actions/cart";
import { useEffect } from "react";
import { getProductsAsync } from "../../redux/actions/products";
import { errorText } from "../NotFound/errorText";
import CheckoutForm from "../../components/form/index"


export function Cart(){
    
    const dispatch = useDispatch()
    const cart = useSelector(state => state.cart)
    const { products, loading, error } = useSelector(state => state.products);
    const cartIds = cart.map(item => item.id);
    const cartProducts = products.filter(product => cartIds.includes(product.article));
    
    useEffect(() => {
        dispatch(getProductsAsync()); 
        dispatch(getCart());
      }, [dispatch]);

      if (error) return (<NotFound text={errorText}/>)

    return (
        <>
        {!loading ?
            cartProducts.length ?
            <>
            <ProductList 
            products={cartProducts} 
            productNum={true}
            type='delete' />
            <CheckoutForm/>
            </>
        : <p>You have not added anything to the cart yet</p>
        : <Loader/>
        }
        
        </>
        
    )
}