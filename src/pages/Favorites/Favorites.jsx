import ProductList from "../../components/productList";
import { Loader } from "../../components/loader";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getFavorites } from "../../redux/actions/favorites";
import { getProductsAsync } from "../../redux/actions/products";
import { NotFound } from "../NotFound/NotFound";
import { errorText } from "../NotFound/errorText";


export function Favorites(){
    
const dispatch = useDispatch();
const { products, loading, error } = useSelector(state => state.products);
const favorites = useSelector( state => state.favorites)
const favProducts = products.filter(product => favorites.includes(product.article) )

useEffect(() => {
  dispatch(getProductsAsync()); 
  dispatch(getFavorites());
}, [dispatch]);
  
if (error) return (<NotFound text={errorText}/>)
    
    return (<> 

    {!loading ?
    favProducts.length ? 
        <ProductList 
        products={favProducts} 
        type='buy'/> 
        : <p>You haven't added anything to your favorites yet</p>
        : <Loader/>}
        </>
    )
}
